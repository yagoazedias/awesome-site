import { h, Component } from 'preact';
import { Router } from 'preact-router';

import Header from './header';
import Home from './home';
import Profile from './profile';
import $ from "jquery";

export default class App extends Component {
	/** Gets fired when the route changes.
	 *	@param {Object} event		"change" event from [preact-router](http://git.io/preact-router)
	 *	@param {string} event.url	The newly routed URL
	 */

	constructor() {
		super();
		this.updateHeaderState = this.updateHeaderState.bind(this);
		this.state = {
			navbarClass: "notActive"
		};
	}

	componentDidMount() {
		$(document).scroll(this.updateHeaderState);
	}

	updateHeaderState() {
		if ($(window).scrollTop() === 0) {
			this.setState({
				navbarClass: "notActive"
			});
		} else {
			this.setState({
				navbarClass: "isActive"
			});
		}
	}

	handleRoute = e => {
		this.currentUrl = e.url;
	};

	render() {
		return (
			<div id="app">
				<Header navbarClass={this.state.navbarClass} />
				<Router onChange={this.handleRoute}>
					<Home path="/" />
					<Profile path="/profile/" user="me" />
					<Profile path="/profile/:user" />
				</Router>
			</div>
		);
	}
}
