import { h } from 'preact';
import style from './style.less';
import Button from '../button/index';

const Banner = () => (
	<section>
		<img class={style.background} src="http://www.contioutra.com/content/uploads/2016/06/aurora-boreal_8.jpg"/>
		<img class={style.profile} src="https://pbs.twimg.com/profile_images/886642709334814720/B8BFL5Tc_400x400.jpg"/>
		<h1 class={style.h1} className="text-center">
			<span>Olá, meu nome é Yago.<br/></span>
			<span>Sou desenvolvedor de software</span>
		</h1>
		<Button isCentered={true} text="Precisa de alguma ajuda?" bgColor="#673ab7" color="white" />
	</section>
);

export default Banner;