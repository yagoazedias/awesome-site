import { h, Component } from 'preact';
import style from './style.less';

import Navbar from '../navbar/index';

export default class Header extends Component {

	returnCorrectStyle() {
		return this.props.navbarClass === "isActive" ? style.isActive : style.notActive;
	}

	render() {
		return (
			<header class={`${style.header} ${this.returnCorrectStyle()}`}>
				<h1>Yago Azedias</h1>
				<Navbar />
			</header>
		);
	}
}
