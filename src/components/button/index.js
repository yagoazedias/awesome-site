import { h } from 'preact';
import style from './style.less';

const Button = (props) => (
	<div style={{textAlign: `${props.isCentered ? "center" : "left"}`}}>
		<a style={{backgroundColor: `${props.bgColor}`, color: `${props.color}`}} class={style.a}>{props.text}</a>
	</div>
);

export default Button;