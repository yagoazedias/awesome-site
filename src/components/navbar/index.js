import { h } from 'preact';
import { Link } from 'preact-router';
import style from './style.less';

const Navbar = () => (
	<nav class={style.nav}>
		<Link href="/">Home</Link>
		<Link href="/profile">About Me</Link>
		<Link href="/works">My Works</Link>
	</nav>
);

export default Navbar;