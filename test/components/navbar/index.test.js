import { h } from 'preact';
import { expect } from 'chai';
import { Link } from 'preact-router';
import style from '../../../src/components/navbar/style.less';

import Navbar from '../../../src/components/navbar';

describe('components/home', () => {
	
	const nav = (
		<nav class={style.nav}>
			<Link href="/">Home</Link>
			<Link href="/profile">About Me</Link>
			<Link href="/works">My Works</Link>
		</nav>
	);

	it('should navbar show the corrects links', () => {
		const header = <Navbar/>;
		expect(header).to.contain(<a href='/'>Home</a>);
		expect(header).to.contain(<a href='/profile'>About Me</a>);
		expect(header).to.contain(<a href='/works'>My Works</a>);
	});

	it('should navbar show the <nav> tag', () => {
		const header = <Navbar/>;
		expect(header).to.equal(nav);
	});
});
