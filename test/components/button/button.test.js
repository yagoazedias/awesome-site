import { h } from 'preact';
import { expect } from 'chai';

import Button from '../../../src/components/button';

describe('components/Header', () => {

	const buttonCentered = <Button isCentered={true} text="Precisa de alguma ajuda?" bgColor="#673ab7" color="white" />;
	const buttonNotCentered = <Button isCentered={false} text="Precisa de alguma ajuda?" bgColor="#673ab7" color="white" />;

	it('should show button centered', () => {
		expect(buttonCentered).to.equal(
			<div style="text-align: center;">
				<a class="a" style="background-color: #673ab7; color: white;">Precisa de alguma ajuda?</a>
			</div>
		);
	});

	it('should show button not centered', () => {
		expect(buttonNotCentered).to.equal(
			<div style="text-align: left;">
				<a class="a" style="background-color: #673ab7; color: white;">Precisa de alguma ajuda?</a>
			</div>
		);
	});
});


