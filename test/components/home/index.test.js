import { h } from 'preact';
import { expect } from 'chai';
import Banner from '../../../src/components/banner/index';

import Home from '../../../src/components/home';

describe('components/home', () => {
	it('should show the banner component', () => {
		const home = <Home/>;
		expect(home).to.contain(<Banner/>);
	});
});
