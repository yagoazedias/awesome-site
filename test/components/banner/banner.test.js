import { h } from 'preact';
import { expect } from 'chai';

import Banner from '../../../src/components/banner';
import style from '../../../src/components/banner/style.less';

describe('components/Header', () => {

	const banner = <Banner/>;

	it('should show the h1 text with spans', () => {
		expect(banner).to.contain(
			<h1 class={style.h1} className="text-center">
				<span>Olá, meu nome é Yago.<br/></span>
				<span>Sou desenvolvedor de software</span>
			</h1>
		);
	});

	it('should show the space iframe', () => {
		expect(banner).to.contain(<iframe class={style.iframe} className="animation" id="iframe-animation" src="http://pixijs.teamgbaws.net/header/" />);
	});

});
